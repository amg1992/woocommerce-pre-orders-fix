=== WooCommerce Pre-Orders Fix ===
Author: amg26
Requires at least: 4.0
Tested up to: 4.6.1
Stable tag: 1.0

== Description ==

Sell pre-orders for products in your WooCommerce store, multiple pre-order cart add-on.

See http://docs.woothemes.com/document/pre-orders/ for full documentation.
